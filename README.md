# Rodando a aplicação em modo de desenvolvimento:

1 - Instale o Mongodb. Você pode encontrar no link a seguir, preste atenção para baixar a versão compatível com seu sistema operacional: `https://www.mongodb.com/download-center/community`;
2 - Após a instalação será necessário que adicione uma variável de ambiente para que o sistema encontre o mongo. Para isso você precisa copiar o caminho da pasta aonde o mongo foi instalado até a pasta bin. No Windows o path será algo parecido como isso `C:\Program Files\MongoDB\Server\4.0\bin`;
    Vá até o Painel de Controle, clique em Sistema e Segurança e depois em Configurações avançadas do sistema. Clique no botão abaixo chamado Variáveis de Ambiente. Em seguida em Variáveis do sistema selecione o ítem chamado Path, em seguida clique em editar. Caso apareça uma tela com vários paths clique em Novo e adicione então o path até a pasta bin mencionado acima. Caso apareça uma janela com 2 campos, no campo chamado Valor da variável vá até o final da linha, adicione um ; e então cole o path.
3 - Instale o Nodejs e adicione nas variáveis de ambiente. O path será algo assim: `C:\Program Files\nodejs\`.
4 - O mongo precisa então que seja criado uma pasta para armazenar as informações do banco. Vá até a pasta c:/ do seu sistema crie uma pasta chamada data, entre nesta pasta e adicione uma nova pasta chamada db;
5 - Usando permissão de administrador abra o terminal e insira o comando: `mongod`. O aplicativo deverá ser inicializado.
6 - Abra o terminal na pasta `backend` e rode o comando `npm i`. Repita o processo na pasta `frontend`.
7 - Usando uma nova instância do terminal vá até a pasta backend e rode o comando: `npm run dev`
    Caso algum erro apareça no console, verifique se a porta em que o nodemon está tentando rodar não está ocupada por outro processo do seu sistema. A porta está definida no arquivo backend/src/config/server.js e é usada em frontend/src/users/userActions.js;
8 - Com o backend rodando, vá até a pasta do frontend e rode o comando: `npm run start`.
9 - Abra seu navegador e acesse o endereço: `http://localhost:8080/`;

obs.: Caso apareça um erro: SyntaxError no arquivo index.js: Unexpected token verificar os arquivos .gitignore e .babelrc. As vezes acontece do sistema remover o ponto e acrescentar outro caractere na frente.