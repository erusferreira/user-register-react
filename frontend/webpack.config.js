const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {   
                test: /\.html$/, 
                use: [
                    { 
                        loader: "html-loader",
                        options: { minimize: true } 
                    }
                ]
            }
        ]
    },
    devServer: {
        port: 8080
    },
    plugins: [
        new HtmlWebPackPlugin({template: './src/index.html', filename: './index.html'}),
        new MiniCssExtractPlugin({filename: "styles.css"})
    ]
}