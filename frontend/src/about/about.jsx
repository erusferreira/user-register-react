import React from 'react';

import PageHeader from '../template/pageHeader.jsx';

export default props => (
    <div>
        <PageHeader name='Sobre' small='Nós'></PageHeader>
        <div className='container'>
            <div className='row'>
                <div className="col-12">
                    <h2>Nossa história</h2>
                    <p>Lorem ipsum dolor sit amet...</p>
                    <p>Lorem ipsum dolor sit amet...</p>
                    <p>Lorem ipsum dolor sit amet...</p>
                </div>
            </div>
        </div>
    </div>
)