import React, {Component} from 'react';

import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

 class RangeInput extends Component {
    constructor(props) {
        super(props);
    }

    onSliderChange(val){
        this.setState({
            value: val,
        });
    }
    
    render() {
        const { input: { value, onChange, onAfterChange }, min, max, step } = this.props;
        return (
            <Slider value={+value}
                    min={min}
                    max={max}
                    step={step}
                    onChange={onChange.bind(this)}
                    onAfterChange={this.onSliderChange.bind(this)}  
                    {...this.props} />
        )
    }
 }

export default RangeInput;
