import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import React, {Component} from 'react';

import Menu from '../template/menu.jsx';
import Routes from './routes.jsx';

class App extends Component{
    render(){
        return(
            <div>      
                <Menu />
                <Routes />
            </div>
        )       
    }
}

export default App;
