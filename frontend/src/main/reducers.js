import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import userReducer from '../users/userReducer';
import { reducer as formReducer} from 'redux-form';

export const createRootReducer = (history) => combineReducers({
    router: connectRouter(history),
    users: userReducer,
    form: formReducer
});
