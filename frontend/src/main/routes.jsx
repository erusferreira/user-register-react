import React from 'react';
import { Route, HashRouter } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import UsersHome from '../users/users-home.jsx';
import UserRegistration from '../users/user-registration.jsx';
import Users from '../users/users.jsx';
import UserRegistered from '../users/user-registered.jsx';
import { history } from '../main/configureStore.js';

export default props => (
    <ConnectedRouter history={history}>
        <HashRouter>
            <Route path='/' exact component={UsersHome} />
            <Route path='/users/' component={Users} />
            <Route path='/registering/' component={UserRegistration} />
            <Route path='/registered/' component={UserRegistered} />
        </HashRouter>
    </ConnectedRouter>
)
