import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import { createRootReducer} from '../main/reducers.js';
import promise from 'redux-promise';
import multi from 'redux-multi';
import thunk from 'redux-thunk';

export const history = createBrowserHistory();
const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
 
export default function configureStore(preloadedState) {
    const store = createStore(
        createRootReducer(history),    
        preloadedState,
        compose(
            applyMiddleware(
                promise, 
                multi, 
                thunk,
                routerMiddleware(history),
            ),
            devTools
        ),
    ) 
    return store
}
