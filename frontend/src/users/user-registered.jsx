import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

import PageHeader from '../template/pageHeader.jsx';

class UserRegistered extends Component {

    getAge() {
        const { age } = this.props.user;
        switch(age) {
            case 1:
                return '13 anos'
            case 2:
                return '20 anos'
            case 3: 
                return '30 anos'
            case 4:
                return '45 anos'
            default:
                return '20 anos';
        }
    };

    hasUser() {
        return !!this.props.user._id;
    }

    editUser() {
        return this.props.history.push('/registering')
    }

    render() {

        const { firstName, lastName, email, stateAddress, interests, phone, newsletter, _id } = this.props.user;

        return (
            <div>
                <PageHeader name='User' small='registered'></PageHeader>
                <div className="container">
                    <div className="row">
                        <div className="col-3">
                            <img src="http://binarycart.com/bclivedemos/01-05-2014/v1/bs-binary-admin/assets/img/find_user.png" />
                            <div className="row justify-content-center">
                                <button className='btn btn-light btn-sm btn-block col-8'>Editar Foto</button>
                                <button className='btn btn-light btn-sm btn-block col-8' onClick={() => this.editUser()}>Editar Perfil</button>
                            </div>
                        </div>
                        <div className="col-9">
                            <p>Eu sou o/a {firstName} {lastName} e eu tenho mais de {this.getAge()} e você pode enviar e-mails para <strong>{email}</strong></p>
                            <p>Eu moro no estado do {stateAddress}. Eu gosto de {interests}. {!!newsletter ? 'Por favor me envie newsletters.': ''}</p>
                            <p>Para me contatar ligue no telefone {phone}</p>
                            <br />
                            <a href="#/users" className='btn btn-primary col-3'>Confirmar</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { 
        user: state.users.user,
        form: state.users.form 
    }
 };

const mapDispatchToProps = dispatch => bindActionCreators({ push }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(UserRegistered);
