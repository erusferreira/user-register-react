import React from 'react';

import PageHeader from '../template/pageHeader.jsx';
import UsersList from './usersList.jsx';

export default props => (
    <div>
        <PageHeader name='Users' small='list'></PageHeader>
        <UsersList />
    </div>   
);
