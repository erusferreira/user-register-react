import React from 'react';

import PageHeader from '../template/pageHeader.jsx';

export default props => (
    <div>
        <PageHeader name='User' small='registration'></PageHeader>
        <a href="#/registering" className='btn btn-primary col-2 ml-5'>Add User</a>
    </div>   
);
