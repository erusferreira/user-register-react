const INITIAL_STATE = { user: {}, list: [] };

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'GOT_USERS':
            return { ...state, list: action.payload };
        case 'SUBMITED_NEW_USER':
            return { ...state, user: action.payload };
        case 'USER_EDITED':
            return { ...state, user: action.payload };
        case 'CLEAR':
            return { ...state };
        default:
            return state;
    }
}
