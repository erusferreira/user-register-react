import React, {Component} from 'react';
import { reduxForm, getFormValues } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect  } from 'react-router-dom';

import { create } from '../users/userActions.js';
import Form from '../users/user-registration-form.jsx';

class UserRegistration extends Component {

    renderRedirect() {
        if (!!this.props.form.userRegistrationForm && 
            !!this.props.form.userRegistrationForm.submitSucceeded &&
            this.props.history.location.pathname === '/registering') {
            return <Redirect to={{pathname: '/registered'}} />
        }
    }

    render() {
        return (
            <div>
                {this.renderRedirect()}
                <Form onSubmit={this.props.create} />
            </div>   
        )
    }
}

const mapStateToProps = state => ({ 
    user: state.users,
    form: state.form,
    formData: getFormValues("userRegistrationForm")(state)
});
const mapDispatchToProps = dispatch => bindActionCreators({ create }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(UserRegistration)
