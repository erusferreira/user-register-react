import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getUsers } from './userActions.js';

class UsersList extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.getUsers();
    }

    getAgeLabel(age) {
        switch(age) {
            case 1:
                return '13-19';
            case 2:
                return '20-29';
            case 3: 
                return '30-45';
            case 4:
                return '45 e acima';
            default:
                return '-';
        }
    }

    render() {
        const { users } = this.props;
        
        return (
            <table className="table container">
                <thead>
                    <tr className="row">
                        <th className="col-3">Name</th>
                        <th className="col-3">Age</th>
                        <th className="col-3">Email</th>
                        <th className="col-3">Phone</th>
                    </tr>
                </thead>
                <tbody>
                {users.map(res => (
                    <tr key={res._id} className="row">
                        <td className="col-3">
                            {res.firstName} {res.lastName}
                        </td>
                        <td className="col-3">
                            {this.getAgeLabel(res.age)}
                        </td>
                        <td className="col-3">
                            {res.email}
                        </td>
                        <td className="col-3">
                            {res.phone}
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        )
    }
};

const mapStateToProps = state => ({users: state.users.list });
const mapDispatchToProps = dispatch => bindActionCreators({ getUsers }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
