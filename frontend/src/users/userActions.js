import axios from 'axios';

const URL = 'http://localhost:3005/api/users';  

let userMetaDataId = null;

export const getUsers = () => {
    return (dispatch, getState) => {
        const request = axios.get(`${URL}`)
            .then(resp => dispatch({ type: 'GOT_USERS', payload: resp.data }));
    }
};

export function create(user) {
    if (!userMetaDataId) {
        return dispatch => {
            axios.post(URL, user)
                .then((response) => {
                    userMetaDataId = response.data._id;
                    return dispatch({ type: 'SUBMITED_NEW_USER', payload: response.data })
                })
        }
    } else {
        const objPayload = {
            address: user.address,
            age: user.age,
            country: user.country,
            email: user.email,
            firstName: user.firstName,
            interests: user.interests,
            lastName: user.lastName,
            newsletter: user.newsletter,
            phone: user.phone,
            stateAddress: user.stateAddress
        };
        return dispatch => {
            axios.put(`${URL}/${user._id}`, objPayload)
                .then((response) => {
                    return dispatch({ type: 'USER_EDITED', payload: response.data })
                });
        }
    }
}

export const clear = () => {
    return [{ type: 'CLEAR' }]
};
