import React, {Component} from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, formValueSelector, getFormValues } from 'redux-form';
import { bindActionCreators } from 'redux';

import './user-registration-form.css';
import PageHeader from '../template/pageHeader.jsx';
import MyRange from '../common/rangeInput.jsx';

class UserRegistrationForm extends Component {      

    render() {

        const rangeProps = {
            defaultValue: 3,
            marks:{
                1: '13-19',
                2: '20-29',
                3: '30-45',
                4: '45 e acima',
            },
            pushable:true,
            allowCross:false,
            min:1,
            max:4,
            step:1,
        };

        const { handleSubmit, pristine, submitting  } = this.props;
        
        return (
            <div>
                <PageHeader name='User' small='registration'></PageHeader>
                <div className="container">
                    <div className="row">
                        <div className="col-3">
                            <img src="http://binarycart.com/bclivedemos/01-05-2014/v1/bs-binary-admin/assets/img/find_user.png" />
                        </div>
                        <div className="col-9">
                            <form role="form" onSubmit={handleSubmit.bind(this)}>
                                <div className="form-group row">
                                    <label className="col-2 col-form-label">Nome</label>
                                    <div className="col-10">
                                        <Field name="firstName" 
                                               component="input"
                                               className="form-control" 
                                               id="first-name" 
                                               placeholder="Entre seu nome" 
                                               />
                                        <Field name="lastName" 
                                            component="input"  
                                            className="form-control" 
                                            id="last-name" 
                                            placeholder="Entre seu sobrenome" />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="input-idade" 
                                        className="col-2 col-form-label">Idade</label>
                                    <div className="col-10">
                                        <Field name="age" 
                                            component={MyRange}
                                            props={rangeProps}
                                            id="input-age" />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="input-email" 
                                        className="col-2 col-form-label">Email</label>
                                    <div className="col-10">
                                        <Field name="email" 
                                            component="input" 
                                            placeholder="david@example.com"
                                            id="input-email" 
                                            className="form-control" />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="input-phone" 
                                        className="col-2 col-form-label">Telefone</label>
                                    <div className="col-10">
                                        <Field name="phone" 
                                            component="input" 
                                            placeholder="(41) 99999-9999"
                                            id="input-phone" 
                                            className="form-control" />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="input-state" 
                                        className="col-2 col-form-label">Estado</label>
                                    <div className="col-10">
                                        <Field name="stateAddress"
                                            component="select"
                                            className="custom-select" 
                                            id="input-state">
                                            <option>Selecione</option>
                                            <option value="PR">PR</option>
                                            <option value="SC">SC</option>
                                            <option value="RS">RS</option>
                                        </Field>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="input-country" 
                                        className="col-2 col-form-label">País</label>
                                    <div className="col-10">
                                        <Field name="country"
                                            component="select"
                                            className="custom-select" 
                                            id="input-country">
                                            <option>Selecione</option>
                                            <option value="brasil">Brasil</option>
                                            <option value="argentina">Argentina</option>
                                        </Field>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="input-address" 
                                        className="col-2 col-form-label">Endereço</label>
                                    <div className="col-10">
                                        <Field name="address"
                                            component="select"
                                            className="custom-select" 
                                            id="input-country">
                                            <option>Selecione</option>
                                            <option value="casa">Casa</option>
                                            <option value="empresa">Empresa</option>
                                        </Field>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <label htmlFor="input-interests" 
                                        className="col-2 col-form-label">Interesse</label>
                                    <div className="col-10">
                                        <Field name="interests" 
                                            component="input" 
                                            className="form-control" 
                                            id="input-interests" 
                                            placeholder="Futebol, Leitura, Volei" />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-2 col-form-label"></div>
                                    <div className="col-10">
                                        <div className="row">
                                            <Field name="newsletter"
                                                component="input" 
                                                type="checkbox"
                                                className="form-control" 
                                                id="input-newsletter"
                                                placeholder="Futebol, Leitura, Volei" />
                                            <label htmlFor="input-newsletter" 
                                                className="col-11 col-form-label">Desejo receber novidade por e-mail.</label>

                                        </div>
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-2 col-form-label"></div>
                                    <div className="col-10">
                                    <button type="submit" 
                                            disabled={pristine || submitting}
                                            className='btn btn-primary'>Salvar</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            </div>  
        )
    }
}

UserRegistrationForm = reduxForm({
    form: 'userRegistrationForm',
    enableReinitialize : true
})(UserRegistrationForm)

const selector = formValueSelector('userRegistrationForm');

const mapStateToProps = (state, props) => {
    
    return {
        user: state.users.user,
        initialValues: state.users.user,
        formData: getFormValues("userRegistrationForm")(state)
    }
 };

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

UserRegistrationForm = connect(mapStateToProps, mapDispatchToProps)(UserRegistrationForm)

export default UserRegistrationForm;
