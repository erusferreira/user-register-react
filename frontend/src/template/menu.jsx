import React from 'react';

export default props => (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="navbar-brand">User Register App</div>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                    <a className="nav-link" href="#/">Registration<span className="sr-only">(current)</span></a>
                </li>
                <li className="nav-item active">
                    <a className="nav-link" href="#/users/">Users<span className="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
    </nav>
)
