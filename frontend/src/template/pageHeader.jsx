import React from 'react';

export default props => (
    <nav className="navbar navbar-dark bg-dark" style={{marginBottom: "20px"}}>
        <span className="navbar-brand mb-0 h1">{props.name} <small>{props.small}</small></span>
    </nav>
)