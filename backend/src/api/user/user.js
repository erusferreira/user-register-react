const restful = require('node-restful');
const mongoose = restful.mongoose;

const userSchema = new mongoose.Schema({
    firstName: { type: String , required: true },
    lastName: { type: String , required: true },
    age: { type: Number , required: true },
    email: { type: String , required: true },
    phone: { type: String , required: true },
    stateAddress: { type: String , required: true },
    country: { type: String , required: true },
    address: { type: String , required: true },
    interests: { type: String , required: true },
    newsletter: { type: Boolean , required: true, default: false }
});

module.exports = restful.model('User', userSchema)